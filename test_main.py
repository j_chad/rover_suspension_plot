import unittest
import numpy as np
from main import WavePlot, wave_calc


class WaveCalcTestCase(unittest.TestCase):
    """Tests for rover suspension plot `main.py`."""

    def test_wave_objects(self):
        """Does class WavePlot return an object?"""
        w = np.sqrt((15 * 1000) / 50)
        x_coord = np.arange(0, 5, 0.01)
        y_def_coord = 0.1 * np.exp(-x_coord * 1) * np.cos(w * x_coord)
        self.assertIsInstance(WavePlot(x_coord, y_def_coord, 'Time (s)', 'Displacement (m)').plot_wave(), WavePlot,
                              msg="Wave plot class does not return object.")

    def test_wave_object_arrays(self):
        """Does wave_calc return two NumPy arrays?"""
        plot_obj = wave_calc(deflection=0.1, mass=50, spring_constant=15, damping_coefficent=1, min_delfection=0.01,
                             sample_rate=0.01, sample_time=5)
        self.assertIsInstance(plot_obj[0].x, np.ndarray)
        self.assertIsInstance(plot_obj[1].x, np.ndarray)


if __name__ == '__main__':
    unittest.main()
