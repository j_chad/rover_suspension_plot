#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt


class WavePlot:
    # define plot objects
    def __init__(self, x, y, x_label, y_label):
        self.plot_title = 'Rover Suspension Displacement'
        self.x = x
        self.y = y
        self.x_label = x_label
        self.y_label = y_label

    def plot_wave(self):
        plt.title(self.plot_title)
        plt.xlabel(self.x_label)
        plt.ylabel(self.y_label)
        plt.grid(True, which='both')
        plt.plot(self.x, self.y)
        plt.show()

        return self


def wave_calc(deflection, mass, spring_constant, damping_coefficent, min_delfection, sample_rate, sample_time):
    # calculate wave data
    k = spring_constant
    L = damping_coefficent
    w = np.sqrt((k * 1000) / mass)  # angular frequency
    a = deflection

    print(f'Spring period of oscillation {np.sqrt(mass / (k*1000))*(2 * np.pi)} s.')

    # numpy arrays for x and y coords
    x_coord = np.arange(0, sample_time, sample_rate)
    y_def_coord = a * np.exp(-x_coord * L) * np.cos(w * x_coord)  # deflection y co-ordinate
    y_velo_coord = a * (-L * np.exp(-x_coord * L) * np.cos(w * x_coord) - np.exp(-x_coord * L) * w * np.sin(w * x_coord))  # velocity y co-ordinate

    return WavePlot(x_coord, y_def_coord, 'Time (s)', 'Displacement (m)').plot_wave(),\
           WavePlot(x_coord, y_velo_coord, 'Time (s)', 'Displacement (m.s-1)').plot_wave()


def main():
    # user inputs
    _initial_deflection = float(input('Initial deflection in m: '))
    _mass = float(input('Mass in kg: '))
    _spring_constant = float(input('Spring constant in kiloNewtons per metre: '))
    _damping_coefficient = float(input('Damping coefficient (unit-less): '))
    _min_deflection = float(input('Deflection that constitutes negligible (m): '))
    _sample_rate = float(input('Plot sampling rate (s): '))
    _sample_time = float(input('Plot time length (s): '))
    wave_calc(deflection=_initial_deflection, mass=_mass, spring_constant=_spring_constant,
              damping_coefficent=_damping_coefficient, min_delfection=_min_deflection,
              sample_rate=_sample_rate, sample_time=_sample_time)


if __name__ == "__main__":
    main()
